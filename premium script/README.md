# Premium AutoScript

Premium autoscript installer used to install SSH, OVPN, and PPTP VPN on your VPS. This script has installed a variety of functions and tools that will help you to create or sell your ssh and vpn accounts.

### Installation:

For Debian 8 x86 & x64

`apt-get -y install wget && wget https://gitlab.com/azli5083/debian8/raw/master/premium%20script/deb8.sh && chmod +x deb8.sh && ./deb8.sh && rm -f deb8.sh && history -c`

### Credits:

Hosting Termurah & VPS-Murah
