# Install Pritunl
echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" > /etc/apt/sources.list.d/mongodb-org-3.2.list
echo "deb http://repo.pritunl.com/stable/apt jessie main" > /etc/apt/sources.list.d/pritunl.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv EA312927
apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv CF8E292A
apt-get -y update
apt-get -y upgrade
apt-get -y install pritunl mongodb-org
systemctl start mongod pritunl
systemctl enable mongod pritunl

# About
clear
echo "Script ini hanya mengandungi :-"
echo "-Pritunl"
echo "-MongoDB"
echo "Pritunl : https://$MYIP2:443"
echo "Sila login ke pritunl untuk step seterusnya"
echo "Sila copy code dibawah untuk Pritunl anda"
pritunl setup-key

