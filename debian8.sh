#!/bin/bash
#go to root
cd
# setting port ssh
cd
sed -i 's/Port 22/Port 22/g' /etc/ssh/sshd_config
sed -i '/Port 22/a Port 143' /etc/ssh/sshd_config
service ssh restart

#webmin 
apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python -y
echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list
wget http://rricketts.com/wp-content/uploads/2015/02/jcameron-key.asc
apt-key add jcameron-key.asc
apt-get update
apt-get install webmin -y
service webmin restart

# install dropbear
apt-get -y install dropbear
sed -i 's/NO_START=1/NO_START=0/g' /etc/default/dropbear
sed -i 's/DROPBEAR_PORT=22/DROPBEAR_PORT=443/g' /etc/default/dropbear
sed -i 's/DROPBEAR_EXTRA_ARGS=/DROPBEAR_EXTRA_ARGS="-p 109 -p 110"/g' /etc/default/dropbear
/etc/init.d/dropbear start
echo "/bin/false" >> /etc/shells
echo "/usr/sbin/nologin" >> /etc/shells
/etc/init.d/dropbear restart
#sed -i 's/DROPBEAR_BANNER=""/DROPBEAR_BANNER="bannerssh"/g' /etc/default/dropbear
cd

#fail2ban
apt-get -y install fail2ban 
service fail2ban restart

#stunnel4
cd
apt-get install stunnel4 -y
cat > /etc/stunnel/stunnel.conf <<END
[ssh]
accept = 442
connect = 127.0.0.1:22
cert = /etc/stunnel/stunnel.pem
END

openssl genrsa -out key.pem 2048 
yes "" | openssl req -new -x509 -key key.pem -out cert.pem -days 1095 
cat key.pem cert.pem >> /etc/stunnel/stunnel.pem 
sed -i -e 's/ENABLED=0/ENABLED=1/' /etc/default/stunnel4 
/etc/init.d/stunnel4 restart

# install neofetch
echo "deb http://dl.bintray.com/dawidd6/neofetch jessie main" | sudo tee -a /etc/apt/sources.list
curl -L "https://bintray.com/user/downloadSubjectPublicKey?username=bintray" -o Release-neofetch.key && sudo apt-key add Release-neofetch.key && rm Release-neofetch.key
apt-get update
apt-get install neofetch



# blockir torrent
iptables -A OUTPUT -p tcp --dport 6881:6889 -j DROP
iptables -A OUTPUT -p udp --dport 1024:65534 -j DROP
iptables -A FORWARD -m string --string "get_peers" --algo bm -j DROP
iptables -A FORWARD -m string --string "announce_peer" --algo bm -j DROP
iptables -A FORWARD -m string --string "find_node" --algo bm -j DROP
iptables -A FORWARD -m string --algo bm --string "BitTorrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "BitTorrent protocol" -j DROP
iptables -A FORWARD -m string --algo bm --string "peer_id=" -j DROP
iptables -A FORWARD -m string --algo bm --string ".torrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "announce.php?passkey=" -j DROP
iptables -A FORWARD -m string --algo bm --string "torrent" -j DROP
iptables -A FORWARD -m string --algo bm --string "announce" -j DROP
iptables -A FORWARD -m string --algo bm --string "info_hash" -j DROP

# download script
cd /usr/bin
wget -O menu "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/menu.sh"
wget -O usernew "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/usernew.sh"
wget -O trial "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/trial.sh"
wget -O hapus "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/hapus.sh"
wget -O cek "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/user-login.sh"
wget -O member "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/user-list.sh"
wget -O delexp "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/delexp.sh"
wget -O resvis "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/resvis.sh"
wget -O speedtest "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/speedtest_cli.py"
wget -O info "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/info.sh"
wget -O about "https://bitbucket.org/blackkcatt/autoscript/raw/cdfff898e8b1caa50b6d422b9f2e89cf1e050a80/about.sh"

echo "0 0 * * * root /sbin/reboot" > /etc/cron.d/reboot

chmod +x menu
chmod +x usernew
chmod +x trial
chmod +x hapus
chmod +x cek
chmod +x member
chmod +x resvis
chmod +x speedtest
chmod +x info
chmod +x delexp
chmod +x about



#wget -O menu "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/menu.sh"
#wget -O usernew "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/usernew.sh"
#wget -O trial "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/trial.sh"
#wget -O hapus "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/hapus.sh"
#wget -O cek "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/user-login.sh"
#wget -O userlimit "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/userlimit.sh"
#wget -O userlimitssh "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/userlimitssh.sh"
#wget -O member "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/user-list.sh"
#wget -O resvis "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/resvis.sh"
#wget -O speedtest "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/speedtest_cli.py"
#wget -O info "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/info.sh"
#wget -O about "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/about.sh"

#chmod +x menu
#chmod +x usernew
#chmod +x trial
#chmod +x hapus
#chmod +x cek
#chmod +x member
#chmod +x resvis
#chmod +x speedtest
#chmod +x info
#chmod +x userlimit
#chmod +x userlimitssh
#chmod +x about

# finishing
#service openvpn restart
service ssh restart
service dropbear restart
service squid3 restart
service fail2ban restart
service webmin restart
rm -rf ~/.bash_history && history -c
echo "unset HISTFILE" >> /etc/profile

# info
clear
echo "Autoscript Include:" | tee log-install.txt
echo "===========================================" | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "Service"  | tee -a log-install.txt
echo "-------"  | tee -a log-install.txt
echo "OpenSSH  : 22, 143"  | tee -a log-install.txt
echo "Dropbear : 110,109, 443"  | tee -a log-install.txt
#echo "Squid3   : 8080, 3128 (limit to IP SSH)"  | tee -a log-install.txt
#echo "webmin : https://$MYIP:10000"  | tee -a log-install.txt
#echo "badvpn   : badvpn-udpgw port 7300"  | tee -a log-install.txt
echo "stunnel4 : ssl port 442"  | tee -a log-install.txt
echo "sila taip menu         (Menampilkan daftar perintah yang tersedia)"  | tee -a log-install.txt
cd
rm -f /root/deb8.sh