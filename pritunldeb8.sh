#!/bin/bash

# go to root
cd

# Install Command
apt-get -y install ufw
apt-get -y install sudo

# Install Pritunl
echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" > /etc/apt/sources.list.d/mongodb-org-3.2.list
echo "deb http://repo.pritunl.com/stable/apt jessie main" > /etc/apt/sources.list.d/pritunl.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv EA312927
apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv CF8E292A
apt-get -y update
apt-get -y upgrade
apt-get -y install pritunl mongodb-org
systemctl start mongod pritunl
systemctl enable mongod pritunl

# Install Squid
apt-get -y install squid3
cp /etc/squid3/squid.conf /etc/squid3/squid.conf.orig
wget -O /etc/squid3/squid.conf "https://github.com/akuazli/centos6/raw/master/squidazli" 
export DEBIAN_FRONTEND=noninteractive
OS=`uname -m`;
MYIP=$(wget -qO- ipv4.icanhazip.com);
MYIP2="s/xxxxxxxxx/$MYIP/g";
sed -i $MYIP2 /etc/squid3/squid.conf;
service squid3 restart

# Change to Time GMT+8
ln -fs /usr/share/zoneinfo/Asia/Kuala_Lumpur /etc/localtime
cd


# About
clear
echo "Script ini hanya mengandungi :-"
echo "-Pritunl"
echo "-MongoDB"
echo "-Squid Proxy Port 8080, 3128"
echo "TimeZone : Malaysia"
echo "Pritunl : https://$MYIP2:443"
echo "Sila login ke pritunl untuk proceed step seterusnya"
echo "Sila copy code dibawah untuk Pritunl anda"
pritunl setup-key




